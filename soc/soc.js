let keccak = require('keccak');
let file = require('../file');
let bmt = require('../bmt');
let secp256k1 = require('secp256k1');
let wallet = require('../unsafewallet');
let common = require('../util/common');
let chunk = require('../chunk');

// TODO: have two values signatureLength and signatureWithRecoveryLength
let signatureLength = 65; // serialized signature length with recovery number
let idLength = 32;

module.exports = {
	Soc: Soc,
	newFromData: newFromData,
	newFromSocChunk: newFromSocChunk,
	digestToSign: digestToSign,
	idLength: idLength,
	signatureLength: signatureLength,
	deserialize: deserialize,
}

function Soc(id, chunk, signer, cb) {
	let self = this;
	if (typeof id === 'string') {
		id = common.hexToArray(id);
		
	}
	if (id.length != 32) {
		throw 'Invalid id length';
	}
	self.id = id;
	self.chunk = chunk;
	self.signer = signer;
	self.callback = cb;
	self.signature = undefined;
	self.owner_address = undefined;
	self.setChunk = function(ch) {
		self.chunk = ch;
	}
	self.setOwnerAddress = function(address) {
		this.owner_address = address;
	};
	if (signer != undefined) {
		self.setOwnerAddress(signer.getAddress('binary'));
	}
	if (chunk != undefined) {
		self.setChunk(chunk);
	}
}

Soc.prototype.getAddress = function() {
	if (this.owner_address == undefined) {
		throw('owner address not set');
	}
	let h = keccak('keccak256');
	h.update(Buffer.from(this.id));
	h.update(Buffer.from(this.owner_address));
	return h.digest();
};

Soc.prototype.getChunk = function() {
	return this.chunk;
}

Soc.prototype.serializeData = function() {
	let dataLength = idLength + signatureLength + bmt.spanSize;
	dataLength += this.chunk.data.length;
	let serializedData = new Uint8Array(dataLength);

	let cursor = 0;
	serializedData.set(this.id, cursor);
	cursor += idLength;

	serializedData.set(new Uint8Array([this.signature.recovery + 31]), cursor);
	cursor++;

	serializedData.set(this.signature.signature, cursor);
	cursor += signatureLength - 1;

	serializedData.set(this.chunk.span, cursor);
	cursor += bmt.spanSize;

	serializedData.set(this.chunk.data, cursor);
	cursor += this.chunk.data.length;

	return serializedData;
};

Soc.prototype.sign = function() {
	let self = this;
	if (self.signer == undefined) {
		throw 'signer not set';
	}
	let digest = digestToSign(self.id, self.chunk.reference);
	self.signature = self.signer.signDigest32(digest);
	if (self.callback != undefined) {
		let soc_address = self.getAddress();
		let soc_data = self.serializeData();
		let ch = new chunk.Content(soc_address, soc_data, undefined);
		ch.setMeta(0, 0);
		self.callback(ch);
	}
};

Soc.prototype.validate = function(r) {
	if (this.owner_address == undefined) {
		throw 'missing owner address';
	}
	let address = this.getAddress();
	if (r.length != address.length) {
		return false;
	}
	for (let i = 0; i < address.length; i++) {
		if (address[i] != r[i]) {
			return false;
		}
	}
	return true;
};

function newFromSocChunk(ch) {
	return deserialize(ch.data);
}

function deserialize(b) {
	let cursor = 0;
	let soc_id = b.slice(cursor, cursor + idLength);
	cursor += idLength;
		
	let recovery = b[cursor] - 31;
	cursor++;

	let signature = b.slice(cursor, cursor + signatureLength - 1);
	cursor += signatureLength - 1;

	let span = b.slice(cursor, cursor + bmt.spanSize);
	cursor += bmt.spanSize;

	let data = b.slice(cursor);
	cursor += data.length;

	// TODO: add method for setting span bytes in bmt
	let h = new bmt.Hasher();
	h.span = span;
	h.update(data);
	let digest = h.digest();

	// TODO: add chunk constructor that calculates the correct level
	let ch = new chunk.Content(digest, data, span);
	ch.setMeta(0, 0);
	s = new Soc(soc_id, ch, undefined);
	s.signature = {
		'signature': Buffer.from(signature),
		'recovery': recovery,
	}
	let dataToSign = digestToSign(soc_id, digest);
	let pubk = secp256k1.recover(Buffer.from(dataToSign), s.signature.signature, s.signature.recovery, false);
	s.owner_address = wallet.publicKeyToAddress(pubk);
	return s;
}

function newFromData(id, data, signer, cb) {
	let s = new Soc(id, undefined, signer, cb);
	let splitter = new file.Splitter(s.setChunk);
	splitter.split(data);
	return s; 
}

function digestToSign(id, reference) {
	let h = keccak('keccak256');
	h.update(Buffer.from(id));
	h.update(Buffer.from(reference));
	let digest =  h.digest();
	return digest;
}
