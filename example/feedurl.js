let f = require("../feed");
let wallet = require("../unsafewallet/wallet.js");
let fs = require("fs");

let privkeybuf = Buffer.alloc(32);
let fd = fs.openSync(process.argv[2], "r");
let r = fs.readSync(fd, privkeybuf, 0, 32, 0);
console.log(r);
fs.closeSync(fd);

let w = new wallet.Wallet(privkeybuf);
let rq = new f.Request(w);
rq.setEpoch(2, 1);

let topic = Buffer.alloc(32);
rq.setTopic(topic);

let data = "foo\n";
rq.setData(Buffer.from(data, "utf-8"));

console.log(rq);
console.log(rq.getDigest());
