var common = require('../util/common');
var keccak = require('keccak');

var topicLength = 32;
var userLength = 20;
var timeLength = 7;
var levelLength = 1;
var headerLength = 8;
var updateMinLength = topicLength + userLength + timeLength + levelLength + headerLength;

zeros = Buffer.alloc(topicLength);

module.exports = {
	Request: Request,
}

function Request(wallet) {
	this.wallet = wallet;
	this.topic = Array.prototype.slice.call(zeros);
	this.epoch = new Epoch();
	this.protocolVersion = 0;
	this.data = undefined;
	this.signature = undefined;
}

Request.prototype.setTopic = function(topic) {
	_assertTopic(topic);
	this.topic = Array.prototype.slice.call(topic);
}

Request.prototype.setEpoch = function(level, t=undefined) {
	if (t !== undefined) {
		this.epoch.time = t;
	}
	this.epoch.level = level;
}

Request.prototype.setData = function(data) {
	if (!Buffer.isBuffer(data)) {
		throw "data must be buffer";
	}
	this.data = Buffer.from(data);
}

function _assertUser(user) {
	if (!Buffer.isBuffer(user)) {
		throw "user must be buffer";
	}
	if (topic.length != userLength) {
		throw "user must be 20 bytes";
	}
}

function _assertTopic(topic) {
	if (!Buffer.isBuffer(topic)) {
		throw "topic must be buffer";
	}
	if (topic.length != topicLength) {
		throw "topic must be 32 bytes";
	}
}

Request.prototype.stringify = function() {
	let userHex = this.wallet.getAddress('hex');
	let topicHex = common.uint8ToHex(this.topic);
	let epochJson = this.epoch.stringify();
	let signatureHex = common.uint8ToHex(this.signature.signature) + common.uint8ToHex(this.signature.recovery);
	return '{"feed":{"topic":"0x' + topicHex + '","user":"0x' + userHex + '","epoch":' + epochJson + ',"protocolVersion":0,"signature":"0x' + signatureHex + '"}}';
}

Request.prototype.queryString = function(short=false) {
	let topicHex = common.uint8ToHex(this.topic);
	let userHex = this.wallet.getAddress('hex');
	let userTopicQueryString = "topic=0x" + topicHex + "&user=0x" + userHex;
	if (short) {
		return userTopicQueryString;
	}
	let epochTime = this.epoch.time;
	let epochLevel = this.epoch.level;
	let signatureHex = common.uint8ToHex(this.signature.signature) + "0" + this.signature.recovery;
	return userTopicQueryString + "&time=" + epochTime + "&level=" + epochLevel + "&signature=0x" + signatureHex + "&protocolVersion=" + this.protocolVersion;
}

Request.prototype._assert = function() {
	if (this.data === undefined) {
		throw "data must be set";
	}
}

Request.prototype.getDigest = function() {

	this._assert();

	let buf = new ArrayBuffer(updateMinLength + this.data.length);
	let view = new DataView(buf);
	let cursor = 0;

	view.setUint8(cursor, this.protocolVersion);	
	cursor += headerLength;

	this.topic.forEach(function(v) {
		view.setUint8(cursor, v);
		cursor++;
	});

	this.wallet.getAddress('bytes').forEach(function(v){ 
		view.setUint8(cursor, v);
		cursor++;
	});

	// time is little-endian
	view.setUint32(cursor, this.epoch.time, true);
	cursor += timeLength;

	view.setUint8(cursor, this.epoch.level);
	cursor++;

	this.data.forEach(function(v) {
		view.setUint8(cursor, v);
		cursor++;
	});

	console.log(buf);

	let hasher = keccak("keccak256");
	hasher.update(Buffer.from(buf));
	return hasher.digest();	
}

Request.prototype.sign = function() {
	let digest = this.getDigest();
	this.signature = this.wallet.signDigest32(digest);
}

function Epoch() {
	this.time = Math.floor(Date.now() / 1000);
	this.level = 31;
}

Epoch.prototype.stringify = function() {
	return JSON.stringify(this);
}


