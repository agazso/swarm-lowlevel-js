let fs = require('fs');
let path = require('path');

let dir = 'dist';
if (!fs.existsSync(dir)) {
	fs.mkdirSync(dir);
}

let files = [
	'bmt/bmt.js',
	'file/file.js',
	'file/join.js',
	'feed/feed.js',
	'unsafewallet/unsafewallet.js',
];

files.forEach((a) => {
	fs.copyFile(a, dir + "/" + path.basename(a), (err) => {
		if (err) throw err;
		console.debug('copied ' + a);
	});
})
