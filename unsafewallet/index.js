let unsafewallet = require('./unsafewallet');

module.exports = {
	Wallet: unsafewallet.Wallet,
	publicKeyToAddress: unsafewallet.publicKeyToAddress,
	newReadOnlyWallet: unsafewallet.newReadOnlyWallet,
};
