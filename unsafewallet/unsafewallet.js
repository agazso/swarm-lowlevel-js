var keccak = require('keccak');
var crypto = require('crypto');
var secp256k1 = require('secp256k1');
var common = require('../util/common');
let elliptic = require('elliptic');

module.exports = {
	Wallet: Wallet,
	publicKeyToAddress: publicKeyToAddress,
	newReadOnlyWallet: newReadOnlyWallet,
}

function Wallet(privateKeyParam) {

	let privateKeyBuffer;

	if (privateKeyParam === undefined) {
		privateKeyBuffer = crypto.randomBytes(32);
	} else {
		if (!Buffer.isBuffer(privateKeyParam)) {
			throw "private key must be buffer";
		} 
		if (privateKeyParam.length != 32) {
			throw "private key must be 32 bytes";
		}
		privateKeyBuffer = Buffer.from(privateKeyParam);
	} 
	// generate random private key if not given
	this.privateKey = Uint8Array.from(privateKeyBuffer);
	this.privateKeyBuffer = privateKeyBuffer;

	let publicKeyBuffer = secp256k1.publicKeyCreate(privateKeyBuffer, false);
	this.publicKey = Uint8Array.from(publicKeyBuffer);

	this.address = publicKeyToAddress(this.publicKey);

	hasher = keccak('keccak256');
	hasher.update(publicKeyBuffer);
	let bzzKeyBuffer = hasher.digest();
	this.bzzkey = Uint8Array.from(bzzKeyBuffer);
}


Wallet.prototype.getPublicKey = function(format='hex') {
	if (format == 'hex') {
		return common.uint8ToHex(this.publicKey);
	} else {
		return this.publicKey;
	}
}

Wallet.prototype.getAddress = function(format='hex') {
	if (format == 'hex') {
		return common.uint8ToHex(this.address);
	} else {
		return this.address;	
	}
}

Wallet.prototype.getBzzKey = function(format='hex') {
	if (format == 'hex') {
		return common.uint8ToHex(this.bzzkey);
	} else {
		return this.bzzkey;
	}
}

Wallet.prototype.getPrivateKey = function(format='hex') {
	if (format == 'hex') {
		return common.uint8ToHex(this.privateKey);
	} else {
		return this.privateKey;
	}
}


Wallet.prototype.signDigest32 = function(buffer) {
	if (!Buffer.isBuffer(buffer)) {
		throw "input to sign must be buffer";
	} else if (!buffer.length == 32) {
		throw "input to sign must be buffer length 32";
	}
//	let ec = new elliptic.ec('secp256k1');
//	let key = ec.keyFromPrivate(this.privateKeyBuffer);
//	let sig = key.sign(buffer);
//	signatureSerial = new Uint8Array(65);
//	signatureSerial[0] = sig.recoveryParam;
//	let r = sig.r.toArray();
//	let s = sig.s.toArray();
//	for (i = 0; i < 32; i++) {
//		signatureSerial[i+1] = r[i];
//	}
//	for (i = 0; i < 32; i++) {
//		signatureSerial[i+33] = s[i];
//	}
	return secp256k1.sign(buffer, this.privateKeyBuffer);
}

function newReadOnlyWallet(publicKey) {
	let w = new Wallet(undefined);
	w.privateKey = undefined;
	w.privateKeyBuffer = undefined;

	w.publicKey = Uint8Array.from(publicKey);

	w.address = publicKeyToAddress(w.publicKey);

	hasher = keccak('keccak256');
	hasher.update(Buffer.from(publicKey));
	let bzzKeyBuffer = hasher.digest();
	w.bzzkey = Uint8Array.from(bzzKeyBuffer);
	return w;
}

function publicKeyToAddress(publicKey) {
	// convert public key buffer data to 
	//let pubKeyNoPrefixArray = Array.prototype.slice.call(publicKeyBuffer, 1);
	let pubKeyNoPrefixArray = publicKey.slice(1);
	let pubKeyNoPrefixBuffer = Buffer.from(pubKeyNoPrefixArray);

	// hash the public key and slice the result to obtain the address
	let hasher = keccak('keccak256');
	hasher.update(pubKeyNoPrefixBuffer);
	let addressBaseBuffer = hasher.digest();
	let addressArray = Array.prototype.slice.call(addressBaseBuffer, 12);
	let addressBuffer = Buffer.from(addressArray);
	return Uint8Array.from(addressBuffer);
}
